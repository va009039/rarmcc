// rarmcc.go 2016/4/21
// remote armcc (mbed compile api client)
package main

import (
	"bitbucket.org/va009039/mbedapi"
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

func main() {
	pUserpass := flag.String("userpass", "", "user password")
	pPasscode := flag.String("pass", "", "pass code")
	pUsername := flag.String("user", "", "user name")
	var prog, platform, output, url string
	var debug, verbose bool
	flag.StringVar(&prog, "prog", "mbed_blinky", "program name or Repo URL")
	flag.StringVar(&platform, "platform", "mbed-lpc1768", "platform")
	flag.StringVar(&output, "o", "", "save file name")
	flag.StringVar(&url, "url", "", "api url")
	flag.BoolVar(&debug, "debug", false, "debug mode")
	flag.BoolVar(&verbose, "verbose", false, "verbose mode")
	flag.Parse()

	if *pUserpass == "" && *pPasscode != "" {
		*pUserpass = os.Getenv(*pPasscode)
	}
	for *pUserpass == "" {
		fmt.Println("userpass?")
		fmt.Scanln(pUserpass)
	}

	client := mbedapi.New(*pUsername, *pUserpass)
	client.SetDebug(debug)
	if url != "" {
		client.SetUrl(url)
	}
	res, err := client.CompileStart(platform, prog)
	if err != nil {
		log.Fatal(err)
	}
	id := res.TaskId()
	for t := time.Now(); time.Since(t).Seconds() < 30; time.Sleep(3 * time.Second) {
		res, err := client.CompileStatus(id)
		if err != nil {
			log.Fatal(err)
		}
		for _, m := range res.Result.Data.NewMessages {
			if m.Message != "" {
				if m.Severity == "error" || m.Severity == "warning" {
					fmt.Printf("%s:%s: %s:%s\n", m.File, m.Line, m.Severity, m.Message)
				} else if m.Type == "Error" {
					fmt.Println(m.Message)
				} else if verbose {
					if m.File != "" {
						fmt.Print(m.File + ": ")
					}
					fmt.Println(m.Message)
				}
			}
		}
		if res.TaskComplete() {
			fmt.Println(res.TaskStatus())
			if res.CompilationSuccess() {
				client.CompileDownload(res, output)
				os.Exit(0)
			}
			break
		}
	}
	os.Exit(1)
}
