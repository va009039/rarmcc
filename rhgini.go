// rhgini.go 2016/4/20
package main

//
// remote hg init
// Creating a repository
//
import (
	"bitbucket.org/va009039/mbedapi"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	var username, userpass, passcode string
	var name, description, visibility, repotype, url string
	var debug bool
	flag.StringVar(&username, "user", "", "user name")
	flag.StringVar(&passcode, "pass", "", "pass code")
	flag.StringVar(&userpass, "userpass", "", "user password")
	flag.StringVar(&name, "name", "", "Repo name")
	flag.StringVar(&description, "description", "", "Repo description")
	flag.StringVar(&visibility, "visibility", "private", "public, unlisted or private")
	flag.StringVar(&repotype, "type", "prog", "prog or lib")
	flag.StringVar(&url, "url", "", "api url")
	flag.BoolVar(&debug, "debug", false, "debug mode")
	flag.Parse()
	userpass = setPassword(userpass, passcode)
	if description == "" {
		description = name
	}

	client := mbedapi.New(username, userpass)
	client.SetDebug(debug)
	if url != "" {
		client.SetUrl(url)
	}
	res, err := client.RepositoryCreate(name, description, visibility, repotype)
	if err != nil {
		log.Fatal(err)
	}
	if res.Error {
		for _, msg := range res.Errors.Repotype {
			log.Println(msg)
		}
		for _, msg := range res.Errors.Name {
			log.Println(msg)
		}
		os.Exit(1)
	}
	fmt.Println(res.CreatedRepositoryUrl)
	os.Exit(0)
}

func setPassword(userpass, passcode string) string {
	if userpass == "" && passcode != "" {
		userpass = os.Getenv(passcode)
	}
	for userpass == "" {
		fmt.Println("userpass?")
		fmt.Scanln(&userpass)
	}
	return userpass
}
